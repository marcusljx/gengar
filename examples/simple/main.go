package main

import "fmt"

//go:generate gengar --declare englishData=file1.txt,koreanData=file2.txt

func main() {
	fmt.Println("English Text:", englishData)
	fmt.Println("Korean Text:", koreanData)
}
