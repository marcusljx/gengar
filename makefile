all:build

CI_COMMIT_TAG ?= dev
BINARY_NAME ?= gengar
LDFLAGS := -X main.Cmd.Version=${CI_COMMIT_TAG}

build: main.go go.mod
	go build -ldflags "${LDFLAGS}" -o bin/${BINARY_NAME}