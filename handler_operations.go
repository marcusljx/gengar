package main

import (
	"bytes"
	"fmt"
	"go/format"
	"go/parser"
	"go/token"
	"os"
	"path/filepath"

	"github.com/golang/glog"
)

func (h *Handler) parseFiles() error {
	for key, file := range h.VarNameToFiles {
		go h.templateData.AddVariable(key, file)
	}
	return nil
}

func (h *Handler) discoverPackageName() error {
	fileAbsPath, err := filepath.Abs(h.OutputFile)
	if err != nil {
		return fmt.Errorf("filepath.Abs : %v", err)
	}
	dir := filepath.Dir(fileAbsPath)

	pkgMap, parseErr := parser.ParseDir(token.NewFileSet(), dir, nil, parser.PackageClauseOnly)
	if parseErr != nil {
		return fmt.Errorf("parser.ParseDir : %v", err)
	}

	if len(pkgMap) == 0 {
		h.templateData.Package = filepath.Base(dir)
		return nil
	}

	for name := range pkgMap {
		h.templateData.Package = name
		break
	}
	return nil
}

func (h *Handler) resolveOutputSink() error {
	switch h.OutputFile {
	case "stdout":
		h.outputSink = os.Stdout
	case "stderr":
		h.outputSink = os.Stderr

	default:
		fd, err := os.Create(h.OutputFile)
		if err != nil {
			return fmt.Errorf("os.Create : %v", err)
		}
		h.outputSink = fd
	}

	return nil
}

func (h *Handler) executeTemplate() error {
	buf := new(bytes.Buffer)

	if err := h.templateData.Generate(buf); err != nil {
		return fmt.Errorf("templateData.Generate : %v", err)
	}

	formattedContent, gofmtErr := format.Source(buf.Bytes())
	if gofmtErr != nil {
		return fmt.Errorf("format.Source : %v", gofmtErr)
	}

	if _, err := fmt.Fprintf(h.outputSink, "%s", formattedContent); err != nil {
		glog.Errorf("fmt.Fprintf : %v", err)
	}
	return nil
}
