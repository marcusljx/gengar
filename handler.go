package main

import (
	"fmt"
	"io"

	"github.com/golang/glog"
	"github.com/spf13/cobra"
	"gitlab.com/marcusljx/gengar/pkg/gen"
)

// Handler represents a generator
type Handler struct {
	BytesPerLine   int
	VarNameToFiles map[string]string
	OutputFile     string

	templateData *gen.TemplateData
	outputSink   io.WriteCloser
}

func (h *Handler) PreRunE(cmd *cobra.Command, _ []string) error {
	if err := h.parseFiles(); err != nil {
		return fmt.Errorf("parseFiles : %v", err)
	}

	if !cmd.Flags().Changed("package") { // if no custom package set
		if err := h.discoverPackageName(); err != nil {
			return fmt.Errorf("discoverPackageName : %v", err)
		}
	}
	return nil
}

func (h *Handler) RunE(_ *cobra.Command, _ []string) error {
	if err := h.resolveOutputSink(); err != nil {
		return fmt.Errorf("resolveOutputSink : %v", err)
	}

	if err := h.executeTemplate(); err != nil {
		return fmt.Errorf("executeTemplate : %v", err)
	}
	return nil
}

func (h *Handler) PostRunE(_ *cobra.Command, _ []string) error {
	defer closeLogError(h.outputSink)
	return nil
}

func closeLogError(closer io.Closer) {
	if err := closer.Close(); err != nil {
		glog.Errorf("io.Closer.Close : %v", err)
	}
}
