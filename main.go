package main

import (
	"os"

	"github.com/golang/glog"
	"github.com/spf13/cobra"
	"gitlab.com/marcusljx/gengar/pkg/gen"
)

const (
	helpText = "Gengar is a static binding library with a single focus on embedding files as byte-slices in go source files"
)

var (
	h = &Handler{
		BytesPerLine:   16,
		VarNameToFiles: make(map[string]string),
		OutputFile:     "file_data.go",
		templateData:   gen.NewTemplateData(),
	}

	Cmd = &cobra.Command{
		Use:     "gengar",
		Example: "gengar --declare myVar=./path/to/file,myVar2=/another/file",
		Short:   "generate a byte slice representing your file",
		Long:    helpText,
		Args:    cobra.NoArgs,

		PreRunE:  h.PreRunE,
		RunE:     h.RunE,
		PostRunE: h.PostRunE,
	}
)

func main() {
	if err := Cmd.Execute(); err != nil {
		glog.Errorf("error : %v", err)
		os.Exit(1)
	}
}

func init() {
	Cmd.Flags().StringVar(&h.templateData.Package, "package", h.templateData.Package, "package name to use when generating the file. Use this to generate the data in a package with a different name.")
	Cmd.Flags().StringVarP(&h.OutputFile, "out", "o", h.OutputFile, "file to write out to")
	Cmd.Flags().StringToStringVarP(&h.VarNameToFiles, "declare", "d", h.VarNameToFiles, "comma-separated list of of var_name=file pairs")
}
