package gen

import (
	"fmt"
	"strings"
)

// DeclareByteSlice returns the declaration of data,
// with the given bytesPerLine
func DeclareByteSlice(data []byte, bytesPerLine int) string {
	var (
		b = &strings.Builder{}
	)
	b.WriteString("[]byte{\n")
	for i := 0; i < len(data); i += bytesPerLine {
		end := i + bytesPerLine
		if end >= len(data) {
			end = len(data)
		}

		for _, byt := range data[i:end] {
			b.WriteString(fmt.Sprintf("0x%02x, ", byt))
		}
		b.WriteRune('\n')
	}
	b.WriteRune('}')
	return b.String()
}
