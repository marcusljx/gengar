package gen

import (
	"fmt"
	"io"
	"io/ioutil"
	"text/template"

	"github.com/golang/glog"
)

type TemplateData struct {
	Package   string
	Variables []*FileData

	tmpl *template.Template
}

type FileData struct {
	Path string
	Name string
	Data []byte
}

func NewTemplateData() *TemplateData {
	result := &TemplateData{
		tmpl: template.New("gengar"),
	}

	result.tmpl.Funcs(map[string]interface{}{
		"DeclareByteSlice": DeclareByteSlice,
	})

	result.tmpl = template.Must(result.tmpl.Parse(string(fileDataTemplate)))
	return result
}

//go:generate gengar --out z_data.go --declare fileDataTemplate=./templates/file_data.gotmpl

func (t *TemplateData) AddVariable(name string, file string) {
	data, err := ioutil.ReadFile(file)
	if err != nil {
		glog.Errorf("ioutil.ReadFile : %v", err)
	}

	t.Variables = append(t.Variables, &FileData{
		Path: file,
		Name: name,
		Data: data,
	})
}

func (t *TemplateData) Generate(w io.Writer) error {
	if err := t.tmpl.Execute(w, t); err != nil {
		return fmt.Errorf("template.Template.Execute : %v", err)
	}
	return nil
}

///*TEMPORARY*/
//var (
//	fileDataTemplate = func() []byte {
//		data, err := ioutil.ReadFile("/home/marcusljx/Repositories/marcusljx/gengar/pkg/gen/templates/file_data.gotmpl")
//		if err != nil {
//			panic(err)
//		}
//		return data
//	}()
//)
