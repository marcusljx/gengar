# gengar
byte-slice (`[]byte`) generator in go for files

## What `gengar` Does
`gengar` focuses on a single purpose: to provide file data as static variables in go source code.

## How To
```bash
# using go get
go get gitlab.com/marcusljx/gengar

# use the tool to get 
gengar -o generated_data.go --declare foo=file1.txt,bar=file2.tmpl
```

## Examples
See [examples](/examples).

## Features
- [x] ~~multiple variables per `var` block~~ 
- [x] ~~formatted output (`gofmt`)~~
- [x] ~~specify custom `package` symbol in output file (good for `var` declarations solely used in tests)~~
- [ ] custom bytes-per-line in `[]byte` declaration
- [ ] custom encoding with runtime decoding (useful for compressing build size)